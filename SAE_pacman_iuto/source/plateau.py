"""
            SAE1.02 PACMAN IUT'O
         BUT1 Informatique 2023-2024

        Module plateau.py
        Ce module contient l'implémentation de la structure de données
        qui gère le plateau jeu aussi qu'un certain nombre de fonctions
        permettant d'observer le plateau et d'aider l'IA à prendre des décisions
"""
import const
import case
import random



def get_nb_lignes(plateau):
    """retourne le nombre de lignes du plateau

    Args:
        plateau (dict): le plateau considéré

    Returns:
        int: le nombre de lignes du plateau
    """
    return max(plateau)[0] + 1


def get_nb_colonnes(plateau):
    """retourne le nombre de colonnes du plateau

    Args:
        plateau (dict): le plateau considéré

    Returns:
        int: le nombre de colonnes du plateau
    """
    return max(plateau)[1] + 1

def pos_ouest(plateau, pos):
    """retourne la position de la case à l'ouest de pos

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire d'entiers donnant la position
    Returns:
        int: un tuple d'entiers
    """
    pos_x, pos_y = pos
    if pos_y == 0:
        pos_y = get_nb_colonnes(plateau) - 1
    else:
        pos_y -= 1
    return (pos_x, pos_y)

def pos_est(plateau, pos):
    """retourne la position de la case à l'est de pos

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire d'entiers donnant la position
    Returns:
        int: un tuple d'entiers
    """
    pos_x, pos_y = pos
    if pos_y+1 >= get_nb_colonnes(plateau):
        pos_y = 0
    else:
        pos_y += 1
    return (pos_x, pos_y)

def pos_nord(plateau, pos):
    """retourne la position de la case au nord de pos

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire d'entiers donnant la position
    Returns:
        int: un tuple d'entiers
    """
    pos_x, pos_y = pos
    if pos_x - 1 < 0:
        pos_x = get_nb_lignes(plateau) - 1
    else:
        pos_x -= 1
    return (pos_x, pos_y)

def pos_sud(plateau, pos):
    """retourne la position de la case au sud de pos

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire d'entiers donnant la position
    Returns:
        int: un tuple d'entiers
    """
    pos_x, pos_y = pos
    if pos_x + 1 >= get_nb_lignes(plateau):
        pos_x = 0
    else:
        pos_x += 1
    return (pos_x, pos_y)

def pos_arrivee(plateau,pos,direction):
    """ calcule la position d'arrivée si on part de pos et qu'on va dans
    la direction indiquée en tenant compte que le plateau est un tore
    si la direction n'existe pas la fonction retourne None
    Args:
        plateau (dict): Le plateau considéré
        pos (tuple): une paire d'entiers qui donne la position de départ
        direction (str): un des caractère NSEO donnant la direction du déplacement

    Returns:
        None|tuple: None ou une paire d'entiers indiquant la position d'arrivée
    """
    if direction == "E":
        return pos_est(plateau, pos)
    elif direction == "N":
        return pos_nord(plateau, pos)
    elif direction == "O":
        return pos_ouest(plateau, pos)
    elif direction == "S":
        return pos_sud(plateau, pos)
    return None

def get_case(plateau, pos):
    """retourne la case qui se trouve à la position pos du plateau

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        dict: La case qui se situe à la position pos du plateau
    """
    return plateau[pos]

def get_objet(plateau, pos):
    """retourne l'objet qui se trouve à la position pos du plateau

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        str: le caractère symbolisant l'objet
    """
    return get_case(plateau, pos)["objet"]

def poser_pacman(plateau, pacman, pos):
    """pose un pacman en position pos sur le plateau

    Args:
        plateau (dict): le plateau considéré
        pacman (str): la lettre représentant le pacman
        pos (tuple): une paire (lig,col) de deux int
    """
    get_case(plateau, pos)["pacmans_presents"] = pacman

def poser_fantome(plateau, fantome, pos):
    """pose un fantome en position pos sur le plateau

    Args:
        plateau (dict): le plateau considéré
        fantome (str): la lettre représentant le fantome
        pos (tuple): une paire (lig,col) de deux int
    """
    get_case(plateau, pos)["fantomes_presents"] = fantome

def poser_objet(plateau, objet, pos):
    """Pose un objet en position pos sur le plateau. Si cette case contenait déjà
        un objet ce dernier disparait

    Args:
        plateau (dict): le plateau considéré
        objet (int): un entier représentant l'objet. const.AUCUN indique aucun objet
        pos (tuple): une paire (lig,col) de deux int
    """
    get_case(plateau, pos)["objet"] = objet

def plateau_from_str(la_chaine, complet=True):
    """Construit un plateau à partir d'une chaine de caractère contenant les informations
        sur le contenu du plateau (voir sujet)

    Args:
        la_chaine (str): la chaine de caractères décrivant le plateau

    Returns:
        dict: le plateau correspondant à la chaine. None si l'opération a échoué
    """
    separe_ligne = []
    add = ""
    for lettre in la_chaine:
        if lettre == "\n":
            separe_ligne.append(add)
            add = ""
        else:
            add += lettre
    nb_ligne, nb_colonne = separe_ligne[0].split(";")
    separe_ligne.pop(0)
    plateau = {}
    nb_ligne, nb_colonne = int(nb_ligne), int(nb_colonne)
    for ligne in range(nb_ligne):
        for colonne in range(nb_colonne):
            mur=False
            objet=const.AUCUN
            pacmans_presents=None
            fantomes_presents=None
            value = separe_ligne[ligne][colonne]
            if value == const.VITAMINE:
                objet = const.VITAMINE
            elif value == "#":
                mur = True
            elif value in const.LES_OBJETS:
                if value == const.VITAMINE:
                    objet = const.VITAMINE
                elif value == const.GLOUTON:
                    objet = const.GLOUTON
                elif value == const.IMMOBILITE:
                    objet = const.IMMOBILITE
                elif value == const.PASSEMURAILLE:
                    objet = const.PASSEMURAILLE
                elif value == const.VALEUR:
                    objet = const.VALEUR
                else:
                    objet = const.TELEPORTATION
            plateau[(ligne, colonne)] = {"mur" : mur, "objet" : objet, "pacmans_presents" : pacmans_presents, "fantomes_presents" : fantomes_presents}
    nb_pacmans = int(separe_ligne[nb_ligne])
    indice = nb_ligne + 1 + nb_pacmans
    for ligne in range(nb_ligne + 1, indice):
        nom, num_ligne, num_colonne = separe_ligne[ligne].split(";")
        num_ligne, num_colonne = int(num_ligne), int(num_colonne)
        if plateau[(num_ligne, num_colonne)]["pacmans_presents"] is None:
            plateau[(num_ligne, num_colonne)]["pacmans_presents"] = {nom}
        else:
            plateau[(num_ligne, num_colonne)]["pacmans_presents"].add(nom)
    nb_fantomes = int(separe_ligne[indice])
    for ligne in range(indice + 1, indice + 1 + nb_fantomes):
        nom, num_ligne, num_colonne = separe_ligne[ligne].split(";")
        num_ligne, num_colonne = int(num_ligne), int(num_colonne)
        if plateau[(num_ligne, num_colonne)]["fantomes_presents"] is None:
            plateau[(num_ligne, num_colonne)]["fantomes_presents"] = {nom}
        else:
            plateau[(num_ligne, num_colonne)]["fantomes_presents"].add(nom)
    return plateau


def Plateau(plan):
    """Créer un plateau en respectant le plan donné en paramètre.
        Le plan est une chaine de caractères contenant
            '#' (mur)
            ' ' (couloir non peint)
            une lettre majuscule (un couloir peint par le joueur représenté par la lettre)

    Args:
        plan (str): le plan sous la forme d'une chaine de caractères

    Returns:
        dict: Le plateau correspondant au plan
    """
    return plateau_from_str(plan)


def set_case(plateau, pos, une_case):
    """remplace la case qui se trouve en position pos du plateau par une_case

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int
        une_case (dict): la nouvelle case
    """
    plateau[pos] = une_case




def enlever_pacman(plateau, pacman, pos):
    """enlève un joueur qui se trouve en position pos sur le plateau

    Args:
        plateau (dict): le plateau considéré
        pacman (str): la lettre représentant le joueur
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        bool: True si l'opération s'est bien déroulée, False sinon
    """
    try:
        pacmans_presents = case.get_pacmans(get_case(plateau, pos))
        pacmans_presents.remove(pacman)
        if len(pacmans_presents) == 0:
            pacmans_presents = None
        return True
    except:
        return False


def enlever_fantome(plateau, fantome, pos):
    """enlève un fantome qui se trouve en position pos sur le plateau

    Args:
        plateau (dict): le plateau considéré
        fantome (str): la lettre représentant le fantome
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        bool: True si l'opération s'est bien déroulée, False sinon
    """
    try:
        fantomes_presents = case.get_fantomes(get_case(plateau, pos))
        fantomes_presents.remove(fantome)
        if len(fantomes_presents) == 0:
            fantomes_presents = None
        return True
    except:
        return False


def prendre_objet(plateau, pos):
    """Prend l'objet qui se trouve en position pos du plateau et retourne l'entier
        représentant cet objet. const.AUCUN indique qu'aucun objet se trouve sur case

    Args:
        plateau (dict): Le plateau considéré
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        str: le caractère représentant l'objet qui se trouvait sur la case.
        const.AUCUN indique aucun objet
    """
    objet = case.prendre_objet(get_case(plateau, pos))
    return objet
        
def deplacer_pacman(plateau, pacman, pos, direction, passemuraille=False):
    """Déplace dans la direction indiquée un joueur se trouvant en position pos
        sur le plateau si c'est possible

    Args:
        plateau (dict): Le plateau considéré
        pacman (str): La lettre identifiant le pacman à déplacer
        pos (tuple): une paire (lig,col) d'int
        direction (str): une lettre parmie NSEO indiquant la direction du déplacement
        passemuraille (bool): un booléen indiquant si le pacman est passemuraille ou non

    Returns:
        (int,int): une paire (lig,col) indiquant la position d'arrivée du pacman 
                   (None si le pacman n'a pas pu se déplacer)
    """
    if direction in directions_possibles(plateau,pos,passemuraille) and pacman in case.get_pacmans(get_case(plateau, pos)):
        new_pos = pos_arrivee(plateau,pos,direction)
        poser_pacman(plateau, pacman, new_pos)
        enlever_pacman(plateau, pacman, pos)
        return new_pos
    else:
        return None

def deplacer_fantome(plateau, fantome, pos, direction):
    """Déplace dans la direction indiquée un fantome se trouvant en position pos
        sur le plateau

    Args:
        plateau (dict): Le plateau considéré
        fantome (str): La lettre identifiant le fantome à déplacer
        pos (tuple): une paire (lig,col) d'int
        direction (str): une lettre parmie NSEO indiquant la direction du déplacement

    Returns:
        (int,int): une paire (lig,col) indiquant la position d'arrivée du fantome
                   None si le joueur n'a pas pu se déplacer
    """
    new_pos = pos_arrivee(plateau, pos, direction)
    if not case.est_mur(plateau[new_pos]) and enlever_fantome(plateau, fantome, pos):
        poser_fantome(plateau, fantome, new_pos)
        return new_pos
    return None



def case_vide(plateau):
    """choisi aléatoirement sur la plateau une case qui n'est pas un mur et qui
       ne contient ni pacman ni fantome ni objet

    Args:
        plateau (dict): le plateau

    Returns:
        (int,int): la position choisie
    """
    liste_positions_vides = []
    for position in plateau.keys():
        la_case = get_case(position)
        if case.get_objet(la_case) == const.AUCUN and case.get_fantomes(la_case) is None and case.get_nb_pacmans(la_case) is None and not case.est_mur(la_case):
            liste_positions_vides.append(position)
    ind_aleatoire = random.randint(len(liste_positions_vides))
    return liste_positions_vides[ind_aleatoire]

def directions_possibles(plateau,pos,passemuraille=False):
    """ retourne les directions vers où il est possible de se déplacer à partir
        de la position pos

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): un couple d'entiers (ligne,colonne) indiquant la position de départ
        passemuraille (bool): indique si on s'autorise à passer au travers des murs

    Returns:
        str: une chaine de caractères indiquant les directions possibles
              à partir de pos
    """
    possible = ""
    if not passemuraille:
        est = case.est_mur(plateau[pos_est(plateau, pos)])
        if not est :
            possible += "E"
        ouest = case.est_mur(plateau[pos_ouest(plateau, pos)])
        if not ouest :
            possible += "O"
        nord = case.est_mur(plateau[pos_nord(plateau, pos)])
        if not nord :
            possible += "N"
        sud = case.est_mur(plateau[pos_sud(plateau, pos)])
        if not sud :
            possible += "S"
    else:
        possible = "EONS"
    return possible


def voisin(plateau, pos, passmuraille=False):
    voisins = set()
    directions = directions_possibles(plateau, pos, passmuraille)
    if "O" in directions:
        voisins.add(pos_ouest(plateau, pos))
    if "E" in directions:
        voisins.add(pos_est(plateau, pos))
    if "N" in directions:
        voisins.add(pos_nord(plateau, pos))
    if "S" in directions:
        voisins.add(pos_sud(plateau, pos))
    return voisins

def pseudo_calque(plateau, pos, direction, distance_max, passmuraille):
    position_effectuee = set()#on met notre position dans les positions déjà effectué
    new_pos = pos_arrivee(plateau, pos, direction)
    dictionnaire = {0 : {new_pos}}#on créer un dictionnaire avec déjà le premier élément
    if direction not in directions_possibles(plateau, pos, passmuraille):
        return None
    i = 0
    while i != distance_max -1:
        for position in dictionnaire[i]:
            if position not in position_effectuee:
                if i+1 not in dictionnaire.keys():
                    dictionnaire[i+1] = set()
                for elem in voisin(plateau, position, passmuraille):
                    if elem not in position_effectuee:
                        dictionnaire[i+1].add(elem)
                position_effectuee.add(position)
        i += 1
    # print(dictionnaire)
    return dictionnaire

def analyse_plateau(plateau, pos, direction, distance_max, passmuraille=False):
    """calcul les distances entre la position pos est les différents objets et
        joueurs du plateau si on commence par partir dans la direction indiquée
        en se limitant à la distance max. Si il n'est pas possible d'aller dans la
        direction indiquée à partir de pos, la fonction doit retourner None

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire d'entiers indiquant la postion de calcul des distances
        distance_max (int): un entier indiquant la distance limite de la recherche
    Returns:
        dict: un dictionnaire de listes. 
                Les clés du dictionnaire sont 'objets', 'pacmans' et 'fantomes'
                Les valeurs du dictionnaire sont des listes de paires de la forme
                    (dist,ident) où dist est la distance de l'objet, du pacman ou du fantome
                                    et ident est l'identifiant de l'objet, du pacman ou du fantome
            S'il n'est pas possible d'aller dans la direction indiquée à partir de pos
            la fonction retourne None
    """ 
    dictionnaire = pseudo_calque(plateau, pos, direction, distance_max, passmuraille)
    if dictionnaire is None:
        return None
    case_actuel = None
    res = {"objets" : [], "pacmans" : [], "fantomes" : []}
    for (clefs, valeurs) in dictionnaire.items():
        for valeur in valeurs:
            case_actuel = get_case(plateau, valeur)
            if case.get_objet(case_actuel) != const.AUCUN:#Ajout des objets dans le dictionnaires
                res["objets"].append((clefs + 1, case.get_objet(case_actuel)))
            if case.get_fantomes(case_actuel) is not None:#Ici l'ajout des fantomes dans le dictionnaires
                for fantome in case.get_fantomes(case_actuel):
                    res["fantomes"].append((clefs + 1, fantome))
            if case.get_pacmans(case_actuel) is not None:#Ici l'ajout des pacmans dans le dictionnaires
                for pacman in case.get_pacmans(case_actuel):
                    res["pacmans"].append((clefs + 1, pacman))
    return res
        
x = open("cartes/test2.txt")
x = x.read()
x = plateau_from_str(x)
print(analyse_plateau(x, (0, 0),'S', 2, True), case.get_objet(get_case(x, (4, 6))))

def inverse_direction(direction):
    if direction == "O":
        return "E"
    elif direction == "E":
        return "O"
    elif direction == "N":
        return "S"
    elif direction == "S":
        return "N"


def prochaine_intersection(plateau,pos,direction):
    """calcule la distance de la prochaine intersection
        si on s'engage dans la direction indiquée

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire d'entiers donnant la position de départ
        direction (str): la direction choisie

    Returns:
        int: un entier indiquant la distance à la prochaine intersection
             -1 si la direction mène à un cul de sac.
    """
    fini = False
    new_pos = pos
    i = 0
    while not fini:
        i += 1
        new_pos = pos_arrivee(plateau, new_pos, direction)
        les_directions = directions_possibles(plateau, new_pos)
        if len(les_directions) == 1:
            fini = True
            i = -1 
        if len(les_directions) == 2 and direction not in les_directions:
            if les_directions[0] == inverse_direction(direction):
                direction = les_directions[1]
            else:
                direction = les_directions[0]
        if len(les_directions) >= 3:
            fini = True
    return i - 1

x = open("cartes/test2.txt")
x = x.read()
x = plateau_from_str(x)
print(prochaine_intersection(x, (5, 6),'S'))

# A NE PAS DEMANDER
def plateau_2_str(plateau):
        res = str(get_nb_lignes(plateau))+";"+str(get_nb_colonnes(plateau))+"\n"
        pacmans = []
        fantomes = []
        for lig in range(get_nb_lignes(plateau)):
            ligne = ""
            for col in range(get_nb_colonnes(plateau)):
                la_case = get_case(plateau,(lig, col))
                if case.est_mur(la_case):
                    ligne += "#"
                    les_pacmans = case.get_pacmans(la_case)
                    for pac in les_pacmans:
                        pacmans.append((pac, lig, col))
                else:
                    obj = case.get_objet(la_case)
                    les_pacmans = case.get_pacmans(la_case)
                    les_fantomes= case.get_fantomes(la_case)
                    ligne += str(obj)
                    for pac in les_pacmans:
                        pacmans.append((pac, lig, col))
                    for fantome in les_fantomes:
                        fantomes.append((fantome,lig,col))
            res += ligne+"\n"
        res += str(len(pacmans))+'\n'
        for pac, lig, col in pacmans:
            res += str(pac)+";"+str(lig)+";"+str(col)+"\n"
        res += str(len(fantomes))+"\n"
        for fantome, lig, col in fantomes:
            res += str(fantome)+";"+str(lig)+";"+str(col)+"\n"
        return res

def fabrique_calque(plateau, pos_depart, distance_max, passmuraille=False):
    """
    Crée un calque à partir de la position de départ, en explorant le plateau
    dans la direction indiquée jusqu'à la distance maximale.

    Args:
        plateau (dict): Le plateau considéré
        pos_depart (tuple): La position de départ (ligne, colonne)
        direction (str): La direction de l'exploration (N, S, E, O)
        distance_max (int): La distance maximale à explorer
        passmuraille (bool): Indique si le calque doit traverser les murs

    Returns:
        dict: Un calque représentant les positions explorées jusqu'à la distance maximale.
    """
    calque = {}  # Dictionnaire pour stocker les positions explorées
    pile = [(pos_depart, 0)]  # Pile pour la recherche en profondeur (DFS)

    while pile:
        pos_actuelle, distance = pile.pop()

        # Vérifier si la position est déjà explorée ou dépasse la distance maximale
        if pos_actuelle in calque or distance > distance_max:
            continue

        # Ajouter la position au calque avec la distance actuelle
        calque[pos_actuelle] = distance

        # Ajouter les voisins à la pile pour exploration ultérieure
        for voisin_pos in voisin(plateau, pos_actuelle, passmuraille):
            pile.append((voisin_pos, distance + 1))

    return calque


def calcul_distance_max(plateau):
    """Calcule la distance maximale possible sur le plateau.

    Args:
        plateau (dict): le plateau considéré

    Returns:
        int: la distance maximale
    """
    nb_lignes = get_nb_lignes(plateau)
    nb_colonnes = get_nb_colonnes(plateau)
    return nb_lignes + nb_colonnes


def plus_court_chemin(plateau, pos_depart, pos_arrivee, distance_max, passmuraille=False):
    """
    Retourne le plus court chemin jusqu'à la position d'arrivée depuis la position de départ
    en suivant la direction indiquée et en respectant la distance maximale.

    Args:
        plateau (dict): Le plateau considéré
        pos_depart (tuple): La position de départ (ligne, colonne)
        pos_arrivee (tuple): La position d'arrivée (ligne, colonne)
        direction (str): La direction de l'exploration (N, S, E, O)
        distance_max (int): La distance maximale à explorer
        passmuraille (bool): Indique si le chemin peut traverser les murs

    Returns:
        list: Liste des positions à travers lesquelles le chemin passe, du départ à l'arrivée.
              None si aucun chemin n'est possible.
    """
    for direction in directions_possibles(plateau, pos_depart, passmuraille):
        



x = open("cartes/test2.txt")
x = x.read()
x = plateau_from_str(x)
print(plus_court_chemin(x, (1, 1), (1, 0), False))