import random
import const
import plateau
import case
import joueur

def calcul_distance_max(le_plateau):
    return (plateau.get_nb_colonnes(le_plateau) + plateau.get_nb_lignes(le_plateau)) // 4

# def direction_la_plus_courte(le_plateau, position_depart, position_arrivee, passmuraille):
#     """Renvoie la direction la plus courte entre deux positions

#     Args:
#         le_plateau (plateau): un plateau de jeu
#         position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 
#         position_arrivee (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

#     Returns:
#         list: Une liste de positions entre position_arrivee et position_depart
#         qui représente un plus court chemin entre les deux positions
#     """
#     position_actuel = position_depart
#     direction_la_plus_courte = random.choice('NSEO')
#     min_distance = 100
#     distance_max = calcul_distance_max(le_plateau)
#     print("avant la boucle")
#     while position_actuel != position_arrivee:
#         min_distance = 100
#         changé = False
#         directions = plateau.directions_possibles(le_plateau, position_actuel, passmuraille)
#         for direction in directions:
#             if plateau.pseudo_calque(le_plateau, position_actuel, direction, distance_max, passmuraille) is not None:
#                 for (distance, tuple_positions) in plateau.pseudo_calque(le_plateau, position_actuel, direction, distance_max, passmuraille).items():
#                     if plateau.pos_arrivee(le_plateau, position_actuel, direction) in tuple_positions:
#                         print("1 :", min_distance)
#                         print("1 :", distance)
#                         print("la longueur", len(plateau.pseudo_calque(le_plateau, position_actuel, direction, distance_max, passmuraille)))
#                         if (len(plateau.pseudo_calque(le_plateau, position_actuel, direction, distance_max, passmuraille)) - distance) < min_distance:
#                             direction_la_plus_courte = direction
#                             min_distance = (len(plateau.pseudo_calque(le_plateau, position_actuel, direction, distance_max, passmuraille)) - distance)
#                             print("changé")
#                             changé = True
#                             print("2:", min_distance)
#                             print("2", distance)
#             if changé:
#                 position_actuel = plateau.pos_arrivee(le_plateau, position_actuel, direction_la_plus_courte)
#         print("pos_actuel", position_actuel)
#         print(position_arrivee)
#     return direction_la_plus_courte
    


def fabriquer_calque(le_plateau, position_depart, passmuraille):
    """Renvoie un dictionnaire de dictionnaire qui contient les positions accessibles depuis position_depart

    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        dict: un dictionnaire de dictionnaire qui contient les positions accessibles depuis position_depart
    """
    calque = dict()
    calque[0] = set()
    deja_vue = set()
    distance_max = calcul_distance_max(le_plateau)
    for distance in range(1, distance_max + 1):
        calque[distance] = set()
        for position in calque[distance - 1]:
            directions = plateau.directions_possibles(le_plateau, position, passmuraille)
            for direction in directions:
                nouvelle_position = plateau.pos_arrivee(le_plateau, position, direction)
                if nouvelle_position not in calque[distance]:
                    if nouvelle_position not in deja_vue:
                        deja_vue.add(nouvelle_position)
                        calque[distance].add(distance)
    return calque



    # calque = dict()
    # calque[0] = set()
    # deja_vue = set()
    # distance_max = calcul_distance_max(le_plateau)
    # print("avant la boucle")
    # for distance in range(1, distance_max + 1):
    #     calque[distance] = set()
    #     for position in calque[distance - 1]:
    #         print(calque[distance - 1])
    #         print("position", position)
    #         directions = plateau.directions_possibles(le_plateau, position, passmuraille)
    #         for direction in directions:
    #             nouvelle_position = plateau.pos_arrivee(le_plateau, position, direction)
    #             print("nouvel_pos", nouvelle_position)
    #             if nouvelle_position not in calque[distance]:
    #                 if nouvelle_position not in deja_vue:
    #                     deja_vue.add(nouvelle_position)
    #                     calque[distance].add(distance)
    # return calque


def fabriquer_chemin(le_plateau, calque, position_depart, position_arrivee):
    """Renvoie une liste de positions qui représente un plus court chemin entre position_depart et position_arrivee

    Args:
        calque (dict): un dictionnaire de dictionnaire qui contient les positions accessibles depuis position_depart
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 
        position_arrivee (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        list: Une liste de positions entre position_arrivee et position_depart
        qui représente un plus court chemin entre les deux positions
    """
    distance_max = calcul_distance_max(le_plateau)
    distance = distance_max
    chemin = [position_arrivee]
    while distance > 0:
        for position in calque[distance]:
            if position in plateau.directions_possibles(le_plateau, chemin[-1], False):
                chemin.append(position)
                distance -= 1
    return chemin



x = open("cartes/test2.txt")
x = x.read()
x = plateau.plateau_from_str(x)
print(fabriquer_calque(x, (1, 1), False))





















def meilleur_choix_pacmans(le_plateau, pos, passmuraille=False):
    try:
        directions = plateau.directions_possibles(le_plateau, pos, passmuraille)
        score = 0
        for direction in directions:
            nouvelle_pos = plateau.pos_arrivee(le_plateau, pos, direction)
            if plateau.get_objet(le_plateau, nouvelle_pos) in const.LES_OBJETS:
                if const.PROP_OBJET[plateau.get_objet(le_plateau, nouvelle_pos)][0] > score:
                    score = const.PROP_OBJET[plateau.get_objet(le_plateau, nouvelle_pos)][0]
                    res = direction
        return res
    except:
        print("random")
        print(directions)
        return random.choice(directions)



def meilleur_choix_fantome(le_plateau, pos, passmuraille):
    directions = plateau.directions_possibles(le_plateau, pos, passmuraille)
    for direction in directions:
        if case.get_pacmans(plateau.get_case(le_plateau, pos)) is not set():
            return direction
        else:
            return meilleur_choix_pacmans(le_plateau, pos)
    return random.choice(directions)
    
        


# x = open("cartes/test2.txt")
# x = x.read()
# x = plateau.plateau_from_str(x)
# #print(meilleur_choix_pacmans(x, (8, 2), False))#position a coté d'un objet
# print(meilleur_choix_fantome(x, (3, 4), False))#position a coté de rien
# # print(direction_la_plus_courte(x, (1, 1), (3, 1), False))


def deplacer_pacmans(pos_depart, pos_arrive):
    pass


