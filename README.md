# Pacman IUT'O

Pacman IUT’O est un jeu librement inspiré du jeu d’arcade Pacman dans lequel chaque joueur contrôle un pacman chargé de récupérer des objets et un fantôme chargé d’attaquer les autres pacmans. Ce jeu avait initialement conçu par Lionel Martin pour le concours des 24h des IUT Informatique organisé à Orléans en 2013. L’objectif de cette SAE est de concevoir une IA qui gère les déplacements du pacman et du fantôme d’une équipe.

Le plateau de jeu est un labyrinthe comportant des couloirs et des murs infranchissables (sauf exception). Le plateau est un tore c’est-à-dire que lorsqu’un pacman ou un fantôme sort du plateau par la droite, il réapparait sur la première colonne à gauche de la même ligne.

Sur le plateau peuvent apparaître différents objets qui rapportent des points et ont éventuellement un pouvoir.

- La vitamine qui rapporte 3 points.
- Le glouton qui rapporte 50 points. Cet objet permet au pacman qui l’obtient d’attaquer les fantômes adverses pendant 20 tours de jeu.
- L’immobilité qui rapporte 50 points et permet d’immobiliser tous les fantômes qui se trouvent à moins de 5 cases du pacman qui l’obtient. Ce pouvoir dure 10 tours.
- Le passemuraille qui rapporte 50 points et qui permet au pacman qui l’obtient de passer au travers des murs pendant 20 tours de jeu.
- Les cerises qui rapportent 100 points mais n’ont pas de pouvoir particulier.
- Le téléporteur qui rapporte 50 points et permet au pacman qui l’obtient de se téléporter sur une case vide du plateau choisie de manière aléatoire.

Lors d’un tour de jeu, chaque joueur choisit une direction (Nord, Sud, Est ou Ouest) pour son pacman et son fantôme. Le serveur choisit un ordre aléatoire des joueurs à chaque tour.

Pour chaque joueur,

1. Il déplace d’abord le pacman. Si l’ordre donné par le joueur est incorrect ou n’est pas possible, le joueur perd un point. Si c’est le quatrième faux mouvement du pacman, celui-ci sera téléporté aléatoirement sur la case vide du plateau. Si le pacman peut se déplacer et que la case d’arrivée contient un objet, celui-ci sera attribué au pacman avec le pouvoir correspondant. Si un ou plusieurs fantômes se trouvent sur la case d’arrivée, le joueur perd 20 points par fantôme. Les points perdus seront donnés aux joueurs à qui appartiennent les fantômes.

2. Ensuite, le serveur déplace le fantôme du joueur. Comme pour le pacman, si l’ordre est incorrect ou le déplacement est impossible, le joueur est pénalisé et le fantôme est téléporté au bout de quatre faux mouvements. Si le déplacement est possible, le joueur à qui appartient le fantôme prend 20 points à chaque pacman qui se trouve sur la case d’arrivée.

Lors de la création de la partie, la durée (en nombre de tours) de celle-ci est fixée. Au début de la partie, les pacmans et les fantômes sont placés aléatoirement sur le plateau. À la fin de la partie, les joueurs sont classés en fonction du nombre de points qu’ils ont accumulés. Attention, les joueurs peuvent se retrouver avec des points négatifs.

## 3 Principes du serveur de jeu

Voici la structure du répertoire `SAE_pacman_iuto` contenu dans l’archive

- le sujet en pdf,
- le fichier `EQUIPE` que vous devrez remplir pour donner un nom à votre groupe et indiquer les membres de votre projet,
- tous les fichiers sources et les fichiers de tests,
- un répertoire `images` contenant les images utilisées par la partie graphique,
- un répertoire `cartes` qui contient différentes cartes utilisées par le serveur de jeu,

Le jeu du tournoi repose sur un serveur de jeu qui effectue les actions suivantes :

- créer un jeu à partir d’une carte qui définit les murs et les couloirs du plateau,
- attendre l’enregistrement de 4 joueurs,
- lancer la partie.

Au cours de la partie, le serveur

- envoie à chaque joueur, l’état du plateau et des joueurs,
- attend les ordres envoyés par chaque joueur,
- applique les ordres de chaque joueur en tirant au sort l’ordre de passage.

Chaque joueur est un programme client qui va se connecter au serveur. Un exemple de programme client vous est fourni dans le fichier `client_joueur.py`. Toute la partie connexion et communication avec le serveur vous est fournie, vous n’aurez qu’à implémenter la fonction `mon_IA` pour déterminer les ordres à envoyer au serveur.

Étant donné qu’un joueur doit indiquer à chaque tour de jeu uniquement la direction de son pacman et de son fantôme, il est très facile de faire un joueur complètement aléatoire (voir le fichier `client_joueur.py`).
